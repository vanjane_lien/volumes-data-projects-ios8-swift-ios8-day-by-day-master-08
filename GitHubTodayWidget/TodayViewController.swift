//
// Copyright 2014 Scott Logic
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import UIKit
import GitHubTodayCommon
import NotificationCenter
import CoreLocation

class TodayViewController: UIViewController, NCWidgetProviding, CLLocationManagerDelegate {
    
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var repoNameLabel: UILabel!
    @IBOutlet weak var myImageView: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    
    var imageList = [UIImage]()
    
    var locationManager : CLLocationManager!
    
    let dataProvider = GitHubDataProvider()
    let mostRecentEventCache = GitHubEventCache(userDefaults: NSUserDefaults(suiteName: "group.GitHubToday"))
    var currentEvent: GitHubEvent? {
        didSet {
            dispatch_async(dispatch_get_main_queue()) {
                if let event = self.currentEvent {
                    self.typeLabel.text = event.eventType.icon
                    self.repoNameLabel.text = event.repoName
                } else {
                    self.typeLabel.text = ""
                    self.repoNameLabel.text = ""
                }
                
                
                
            }
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
// Animation
    func startAnimation() -> Void {
        if !myImageView.isAnimating(){
            myImageView.animationImages = imageList
            myImageView.animationDuration = 30.0
            myImageView.startAnimating()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view from its nib.
        currentEvent = mostRecentEventCache.mostRecentEvent
       
        
        var flowerName : NSArray = ["cool_flower-wide.jpg", "flower-power-beauty-products-ftr.jpg", "flowers-beautifull-nice-6.jpg", "Flowers-White-Chamomiles-Wallpaper-Photography.jpg", "Little-Purple-Flowers.jpg", "Red-Butiful-Flowers-Beautiful-Flower-374937.jpg", "Soul-Calibur-Flower-Free-Child-Of-Eden-Gallery-Best-Game-767985.jpg"]
        for i in 0...6
        {
            let imageName = "\(flowerName.objectAtIndex(i))"
            self.imageList.insert((UIImage (named: imageName)), atIndex: i)
        }
        
        
        self.startAnimation()
    }
    
// Location delegate
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        println("locations = \(locations)")
        locationLabel.text = "\(locations)"
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("WIDGET get locations = false")
        locationLabel.text = "WIDGET get locations = false"
    }

    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        println ("WIDGET memory warning")
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)!) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encoutered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        locationManager.startUpdatingLocation()
        
        dataProvider.getEvents("sammyd", callback: {
            events in
            let newestEvent = events[0]
            if newestEvent != self.currentEvent {
                self.currentEvent = newestEvent
                self.mostRecentEventCache.mostRecentEvent = newestEvent
                completionHandler(.NewData)
            } else {
                completionHandler(.NoData)
            }
            
        })
    }
    
    func widgetMarginInsetsForProposedMarginInsets(defaultMarginInsets: UIEdgeInsets) -> UIEdgeInsets {
        let newInsets = UIEdgeInsets(top: defaultMarginInsets.top, left: defaultMarginInsets.left-30,
            bottom: defaultMarginInsets.bottom, right: defaultMarginInsets.right)
        return newInsets
    }
    
    
    /// open app
    @IBAction func handleMoreButtonTapped(sender: AnyObject) {
        let url = NSURL(scheme: "githubtoday", host: nil, path: "/\(currentEvent?.id)")
        extensionContext?.openURL(url, completionHandler: nil)
    }
    
}
