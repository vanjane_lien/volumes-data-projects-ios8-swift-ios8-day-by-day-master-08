//
//  ShareViewController.h
//  ShareExtension
//
//  Created by Thuy Lien Nguyen on 9/25/14.
//  Copyright (c) 2014 ShinobiControls. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>

@interface ShareViewController : SLComposeServiceViewController

@end
